# Q4GD: Pong Tutorial and Source Code

```text
   ____                  _       _____
  / __ \                | |  _  |  __ \
 | |  | |_   _  ___  ___| |_(_) | |__) |__  _ __   __ _
 | |  | | | | |/ _ \/ __| __|   |  ___/ _ \| '_ \ / _` |
 | |__| | |_| |  __/\__ \ |_ _  | |  | (_) | | | | (_| |
  \___\_\\__,_|\___||___/\__(_) |_|   \___/|_| |_|\__, |
                                                   __/ |
                                                  |___/
```

> This Pong game development guide is part of the [Quest for Game Dev](https://quintinhenn.gitlab.io/quest-for-game-dev/) series.

## :mortar_board: How to Create a Pong Game

The [Quest: Pong](https://quest-for-game-dev.gitlab.io/quest-pong-tutorial/) is a guide on how to create a Pong game using your favorite programing langauge with example games in Java, Python, Lua, C# and many others.

## :video_game: Pong Game Source Code

The [source code](source/) should prove useful in understanding how to create your own Pong game.

## Where to Find What

```folders
.
├── assets/ -- game art and sound assets
├── game/ -- playable game binaries
├── docs/ -- game design documentation
├── source/ -- all the source code for the game
│   ├── java/
│   ├── lua/
│   ├── python/
│   └── ruby/
└── tutorial/ -- the tutorial project

```

## :page_with_curl: The License

The tutorial content and all the game projects are licensed under the GNU GPLv3 License -- see the [LICENSE](LICENSE) file for details.
